# tcpdump container image

Containerized `tcpdump` based on the `alpine:edge` image.

## Usage example

```shell
docker run \
    --rm -t -i \
    --network=host \
    -v /tmp/tcpdump:/data \
    registry.gitlab.com/tmaczukin-docker/tcpdump:latest \
    host dl-cdn.alpinelinux.org
```

## Author

Tomasz Maczukin, 2021

## License

MIT

