FROM golang:1.17-alpine AS builder

WORKDIR /src
COPY . /src/

RUN go build -o tcpdump .

FROM alpine:edge

VOLUME /data
EXPOSE 5000

RUN apk add --no-cache tcpdump coreutils

COPY --from=builder /src/tcpdump /

ENTRYPOINT ["/tcpdump"]
