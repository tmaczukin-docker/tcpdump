package main

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"net/http/httptest"
	"os"
	"os/exec"
	"os/signal"
	"path/filepath"
	"strconv"
	"sync"
	"syscall"

	"github.com/sirupsen/logrus"
)

const (
	defaultDataDir  = "/data"
	dumpFilePattern = "dump.pcap."

	listenPort = 5000
)

var (
	dataDir string

	debug = false
	server = false
)

func init() {
	dataDir = os.Getenv("DATA_DIR")
	if dataDir == "" {
		dataDir = defaultDataDir
	}

	dbg, err := strconv.ParseBool(os.Getenv("DEBUG"))
	if err == nil {
		debug = dbg
	}

	srv, err := strconv.ParseBool(os.Getenv("SERVER"))
	if err == nil {
		server = srv
	}
}

func main() {
	log := logrus.New()

	log.SetLevel(logrus.InfoLevel)
	if debug {
		log.SetLevel(logrus.DebugLevel)
	}

	ctx, cancelFn := startInterruptHandler(context.Background(), log)

	wg := new(sync.WaitGroup)

	wg.Add(2)

	cmdCtx := startManagementServer(ctx, wg, log, cancelFn)
	startTCPDump(cmdCtx, wg, log, os.Args[1:])

	log.Info("Awaiting for termination")
	wg.Wait()

	log.Info("Termination done; exiting!")
}

func startInterruptHandler(ctx context.Context, log logrus.FieldLogger) (context.Context, func()) {
	log.Info("Starting interrupt handler")

	newCtx, cancelFn := context.WithCancel(ctx)

	sigCh := make(chan os.Signal)
	signal.Notify(sigCh, os.Interrupt, syscall.SIGTERM)

	go func() {
		select {
		case sig := <-sigCh:
			log.WithField("signal", sig).Warning("Received exit signal; terminating...")
			cancelFn()
		case <-newCtx.Done():
		}
	}()

	return newCtx, cancelFn
}

func startManagementServer(ctx context.Context, wg *sync.WaitGroup, log logrus.FieldLogger, cancelFn func()) context.Context {
	if !server {
		return ctx
	}

	log.Info("Starting management server")

	l, err := net.Listen("tcp", fmt.Sprintf(":%d", listenPort))
	if err != nil {
		log.WithError(err).Error("Error on tcpdump execution")
		cancelFn()
		return nil
	}

	cmdCtx, stopFn := context.WithCancel(ctx)

	handler := http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		log.WithFields(logrus.Fields{
			"proto":       request.Proto,
			"host":        request.Host,
			"method":      request.Method,
			"request_uri": request.RequestURI,
			"remote_addr": request.RemoteAddr,
		}).Debug("Received HTTP request")

		if request.Method == "GET" && request.RequestURI == "/stop-tcpdump" {
			stopFn()
			writer.WriteHeader(http.StatusOK)
			return
		}

		if request.Method == "GET" && request.RequestURI == "/exit" {
			cancelFn()
			writer.WriteHeader(http.StatusOK)
			return
		}

		writer.WriteHeader(http.StatusNotFound)
	})

	server := httptest.NewUnstartedServer(handler)
	server.Listener = l

	go func() {
		defer wg.Done()

		server.Start()
		<-ctx.Done()

		server.Close()
	}()

	return cmdCtx
}

func startTCPDump(ctx context.Context, wg *sync.WaitGroup, log logrus.FieldLogger, args []string) {
	defaultArgs := []string{
		"-C", "1000",
		"-W", "100",
		"-w", filepath.Join(dataDir, dumpFilePattern),
		"-U",
		"--print",
		"--number",
		"-v",
	}
	args = append(defaultArgs, args...)

	log.WithField("args", args).Info("Starting tcpdump")

	cmd := exec.CommandContext(ctx, "tcpdump", args...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	go func() {
		defer wg.Done()

		err := cmd.Run()
		if err != nil {
			log.WithError(err).Error("Error on tcpdump execution")
		}
	}()
}
